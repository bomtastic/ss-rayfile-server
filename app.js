// app.js
class AppBootHook {
  constructor(app) {
    this.app = app;
    app.beforeStart(() => {
      // this.app.emit('error', new Error('This is a sample error'));
      console.log('beforeStart init');
    });
    const errorHandle = require('./app/middleware/error_handler.js')({}, this.app);
    this.app.use(errorHandle);
    console.log('AppBootHook init');
  }
  async didReady() {
    // 请将您的 app.ready 中的代码置于此处。
    console.log('didReady init');

  }
}


module.exports = AppBootHook;

