module.exports = app => {
  const { STRING, DATE, INTEGER } = app.Sequelize;
  const Folder = app.model.define('ev_folder', {
    folder_id: { type: STRING, primaryKey: true, autoIncrement: false },
    folder_name: {
      type: STRING,
      comment: '文件夹名称',
      unique: true,
    },
    file_type: { defaultValue: '0', type: STRING, comment: '文件类型' },
    user_id: { type: STRING, comment: '用户id' },
    parent_folder_id: {
      type: STRING,
      comment: '父文件夹id',
      unique: true,
    },
    path: { type: STRING, comment: '文件夹虚拟路径' },
    depth: { type: INTEGER, comment: '文件夹深度' },
    create_time: DATE,
    modify_time: DATE,
  });
  return Folder;
};
