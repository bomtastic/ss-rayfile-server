module.exports = app => {
  const { STRING, DATE, BOOLEAN } = app.Sequelize;
  const Code = app.model.define('ev_code', {
    email: { type: STRING, primaryKey: true, autoIncrement: false },
    code: STRING,
    time: DATE,
    isVerificate: BOOLEAN,
  });
  return Code;
};
