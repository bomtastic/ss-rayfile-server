const ShareFiles = require('./sharedFiles');
module.exports = app => {
  const { STRING, DATE, BOOLEAN } = app.Sequelize;
  const Share = app.model.define('ev_share', {
    share_id: { type: STRING, primaryKey: true, autoIncrement: false },
    user_id: { type: STRING, comment: '用户id' },
    create_time: DATE,
    over_time: DATE,
    share_link: { type: STRING, comment: '分享链接' },
    views: { type: STRING, comment: '查看次数' },
    can_download: { type: BOOLEAN, comment: '是否可下载' },
    share_pwd: { type: STRING, comment: '密码' },
  });
  // Share.share_id(ShareFiles, { foreignKey: 'share_id' });
  return Share;
};
