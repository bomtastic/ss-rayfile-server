module.exports = app => {
  const { STRING, DATE, BIGINT, BOOLEAN } = app.Sequelize;
  const File = app.model.define('ev_file', {
    file_id: { type: STRING, primaryKey: true, autoIncrement: false },
    file_name: {
      type: STRING,
      comment: '原文件名称',
      unique: true,
    },
    file_name_upload: {
      type: STRING,
      comment: '上传文件名称',
      unique: true,
    },
    file_path: { type: STRING, comment: '文件路径' },
    file_size: { type: BIGINT, comment: '文件大小' },
    file_type: { type: STRING, comment: '文件类型' },
    file_suffix: { type: STRING, comment: '文件后缀' },
    user_id: { type: STRING, comment: '用户id' },
    create_time: DATE,
    modify_time: DATE,
    parent_folder_id: { type: STRING, comment: '父文件夹id' },
    path: { type: STRING, comment: '文件虚拟路径' },
    hash: { type: STRING, comment: '文件hash' },
    is_share: { type: BOOLEAN, comment: '是否是转存文件' },
  });
  return File;
};
