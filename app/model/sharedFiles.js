module.exports = app => {
  const { STRING, INTEGER } = app.Sequelize;
  const ShareFiles = app.model.define('ev_shared_files', {
    shared_file_id: { type: INTEGER, primaryKey: true, autoIncrement: true },
    share_id: { type: STRING, comment: '分享id' },
    file_id: { type: STRING, comment: '文件id' },
    folder_id: { type: STRING, comment: '文件夹id' },
  });


  ShareFiles.associate = function() {
    // 用belongsToMany指定多对多关联关系，并指定外键
    app.model.File.belongsToMany(app.model.Share, {
      through: ShareFiles,
      foreignKey: 'file_id',
      otherKey: 'share_id',
    });
    app.model.Share.belongsToMany(app.model.File, {
      through: ShareFiles,
      foreignKey: 'share_id',
      otherKey: 'file_id',
    });
  };
  return ShareFiles;
};
