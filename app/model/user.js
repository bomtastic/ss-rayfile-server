// module.exports = app => {
//   const mysql = app.mysqls;
//   const Schema = mysql.Schema;
//   const UserSchema = new Schema({
//     user_name: { type: String },
//     password: { type: String },
//   });
//   return mysql.model('User', UserSchema);
// };

module.exports = app => {
  const { STRING, DATE, BOOLEAN } = app.Sequelize;
  const User = app.model.define('ev_user', {
    user_id: { type: STRING, primaryKey: true, autoIncrement: false },
    user_name: {
      type: STRING,
      comment: '用户名称',
      unique: true,
    },
    phone: { type: STRING, comment: '用户手机', unique: true },
    email: {
      type: STRING,
      comment: '邮箱',
      unique: true,
      validate: {
        isEmail: true, // 验证是否是有效的电子邮件地址
      },
    },
    password: { type: STRING, allowNull: false, defaultValue: '' },
    avatar_url: { type: STRING, comment: '头像链接' },
    register_time: DATE,
    last_login_time: DATE,
    status: {
      type: BOOLEAN,
      defaultValue: 1,
      allowNull: false,
      comment: '状态',
    },
  });
  return User;
};
