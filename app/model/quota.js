module.exports = app => {
  const { STRING, BIGINT } = app.Sequelize;
  const Quota = app.model.define('ev_quota', {
    user_id: { type: STRING, primaryKey: true, autoIncrement: false },
    total_space: BIGINT,
    used_space: BIGINT,
  });
  return Quota;
};
