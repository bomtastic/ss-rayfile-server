'use strict';

const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const fs = require('fs');
const mime = require('mime-types');

// 加密用户密码
const genSaltPassword = pwd => {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(pwd, salt, (err, hash) => {
        if (!err) resolve(hash);
        else reject(err);
      });
    });
  });
};
// 比较用户密码
/**
 *
 * @param {未加密密码} _pwd
 * @param {已加密密码} pwd
 * @return boolean 是否匹配
 */
const comparePassword = (_pwd, pwd) => {
  return new Promise((resolve, reject) => {
    bcrypt.compare(
      10,
      (_pwd,
      pwd,
      (err, isMatch) => {
        if (!err) resolve(isMatch);
        else reject(err);
      }),
    );
  });
};

/* 生成6位随机数 */
const randomCode = () => {
  let code = '';
  for (let i = 0; i < 6; i++) {
    code += parseInt(String(Math.random() * 10));
  }
  return code;
};
const regEmail = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;

const sendEmail = function (EMAIL, code) {
  console.log(this.app.config.email, 'this.app.config.email');
  const transport = nodemailer.createTransport(this.app.config.email);
  // let EMAIL = req.body.e_mail //req为请求体对象 我使用的是post请求方式，所以通过req.body获取用户提交的邮箱
  if (regEmail.test(EMAIL)) {
    // 邮箱验证通过
    transport
      .sendMail({
        from: 'jwt1797247040@163.com', // 发件邮箱
        to: EMAIL, // 收件列表
        subject: '验证你的电子邮件', // 标题
        html: `
       <p>你好！</p>
       <p>您正在注册SSRayfile社区账号</p>
       <p>你的验证码是：<strong style="color: #ff4e2a;">${code}</strong></p>
       <p>***该验证码10分钟内有效***</p>`, // html 内容
      })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err, '发送验证码错误！');
      })
      .finally(onfinally => {
        transport.close(); // 如果没用，关闭连接池
      });
    // ....验证码发送后的相关工作
  } else {
    console.log(false, 422, '请输入正确的邮箱格式！');

    // assert(false, 422, '请输入正确的邮箱格式！')
  }
};

// 判断目录是否存在，返回 Promise
const dirExist = dir => {
  return fs.promises
    .access(dir, fs.constants.F_OK)
    .then(() => true)
    .catch(() => false);
};

// 0:目录,1:视频,2:音频,3:文档,4:图片,5:其他
const tranFileType = type => {
  // const extension = mime.extension(type);
  switch (type) {
    case 'png':
    case 'jpg':
    case 'jpeg':
    case 'gif':
    case 'bmp':
    case 'webp':
      return 4;
    case 'mp4':
    case 'avi':
    case 'mov':
    case 'wmv':
      return 1;
    case 'mp3':
    case 'wav':
    case 'flac':
    case 'm4a':
      return 2;
    case 'pdf':
    case 'doc':
    case 'docx':
    case 'xls':
    case 'xlsx':
    case 'ppt':
    case 'pptx':
    case 'txt':
      return 3;
    default:
      return 5;
  }
};

module.exports = {
  genSaltPassword,
  comparePassword,
  randomCode,
  sendEmail,
  dirExist,
  tranFileType,
};
