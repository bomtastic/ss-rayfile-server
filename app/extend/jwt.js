// app/middleware/jwt.js
module.exports = (options, app) => {
  return async (ctx, next) => {
    console.log(app, options, 'app');
    const { authorization } = ctx.header;
    if (authorization) {
      try {
        // 从 Authorization 头中获取 JWT，并验证它
        const token = authorization.split(' ')[1];
        console.log(token, 'token');
        ctx.state.user = app.jwt.verify(token, app.config.jwt.secret);
      } catch (err) {
        console.log(err, 'err');
        ctx.throw(401, 'Invalid token');
      }
    } else {
      ctx.throw(401, 'No authorization header');
    }
    await next();
  };
};
