// 'use strict';

const { Service } = require('egg');
const uuid = require('node-uuid');
const bcryptjs = require('bcryptjs');
const { Op } = require('sequelize');

class UserService extends Service {
  async getAllUser() {
    const { ctx } = this;
    const res = await ctx.model.User.findAll({});
    return res;
  }
  //  用户注册
  async userRegister(params) {
    const { ctx } = this;
    const { user_name, encipherPassword: password, email, phone } = params;
    console.log(user_name, password, email);
    const isUserExist = await ctx.model.User.findAll({
      where: {
        [Op.or]: [{ user_name }, { email }],
      },
    });
    // console.log(isUserExist, 'res---------------------------', email);
    if (isUserExist.length) return { msg: '账号已存在!请重新注册' };
    const uuid_rep = uuid.v4();
    const user_obj = {
      user_id: uuid_rep,
      user_name,
      email,
      password,
      register_time: new Date(),
      status: 1,
      phone,
    };
    const user_create = await ctx.model.User.create(user_obj);
    console.log(user_create, 'result');
    if (user_create) {
      const quota_once = await ctx.model.Quota.create({
        user_id: uuid_rep,
        total_space: 5 * 1024 * 1024 * 1024,
        used_space: 0,
      });
      // console.log(quota_once, 'quota_once');
      if (quota_once) return { msg: '注册成功' };
    } else return { msg: '注册用户失败,稍后再试' };
  }
  async sendEmail(params) {
    const { ctx, app } = this;
    const { email } = params;
    const code = ctx.helper.randomCode();
    const isBinding = await ctx.model.User.findAll({
      where: {
        email,
      },
    });
    // console.log(isBinding,'isBinding');
    if (isBinding.length) return { isBinding: 1, msg: '邮箱已绑定账号，请更换邮箱绑定！' };
    // const isCode = await ctx.model.Code.findAll({
    //   where: {
    //     email,
    //   },
    // });
    // if (!isCode.length) {
    //   const Code_once = await ctx.model.Code.create({
    //     code,
    //     email,
    //     time: new Date(),
    //     isVerificate: 0,
    //   });
    //   if (Code_once) {
    //     ctx.helper.sendEmail(email, code);
    //     return { isBinding: 0, msg: '验证码已发送!请10分钟之内完成验证' };
    //   }
    // } else return { isBinding: 0, msg: '验证码已发送!请勿重复发送' };
    const codeExists = await app.redis.exists(email);
    if (codeExists) return { isBinding: 0, msg: '验证码已发送!请勿重复发送' };
    ctx.helper.sendEmail(email, code);
    app.redis.set(email, code, 'EX', 10 * 60);
    return { isBinding: 0, msg: '验证码已发送!请10分钟之内完成验证' };
  }
  async valEmail(params) {
    const { ctx, app } = this;
    const { code, email } = params;
    // const code_once = await ctx.model.Code.findAll({
    //   where: {
    //     email,
    //   },
    // });
    // const valTime = Date.now() - new Date(code_once[0].time).getTime() <= 10 * 60 * 1000;
    const send_code = await app.redis.get(email);
    console.log(send_code, 'send_code');
    return {
      valEmail: code === send_code ? 1 : 0,
      isExpired: send_code ? 0 : 1,
    };
  }
  //  用户登录
  async userLogin(params) {
    const { ctx, app } = this;
    const { email, password } = params;
    // const res = await app.mysql.get('ev_user', { user_name: params.user_name });
    const res = await ctx.model.User.findOne({
      where: {
        email,
      },
    });
    console.log(res, 'res');
    if (!res) return { msg: '账号不存在' };
    const compareResult = bcryptjs.compareSync(password, res.password);
    if (!compareResult) return { msg: '账号或密码错误' };
    // const tran = Object.entries(res.dataValues).filter(([key, val]) => key !== 'password');
    // const user_info = Object.fromEntries(tran);
    // await res.update({ last_login_time: new Date() });
    const user_storage_info = await ctx.model.Quota.findOne({
      where: {
        user_id: res.user_id,
      },
    });
    console.log(user_storage_info);
    await ctx.model.User.update(
      { last_login_time: new Date() },
      {
        where: {
          email,
        },
      },
    );
    // const chinaTime = new Date().toLocaleString('zh-CN', { timeZone: 'Asia/Shanghai' });
    // console.log(chinaTime, 'new Date()');
    const userData = res.dataValues;
    delete userData.password; // 删除密码属性
    console.log(
      ctx.app.jwt.sign(
        {
          data: {
            user_id: res.user_id,
            user_name: res.user_name,
          },
        },
        app.config.jwt.secret,
      ),
      'token',
    );
    return {
      user_info: { ...user_storage_info.dataValues, ...userData },
      token:
        'Bearer ' +
        ctx.app.jwt.sign(
          {
            data: {
              user_id: res.user_id,
              user_name: res.user_name,
              // timestamp: new Date().getTime(),
            },
          },
          app.config.jwt.secret,
        ),
    };

    //  res.resp({ user_name, token: 'Bearer ' + token.create({user_id, user_name }) }, '登录成功')
    // return compareResult;
  }
  async getAcount(params) {
    const { ctx } = this;
    const { user_id } = params;
    const userInfo = await ctx.model.User.findOne({
      where: {
        user_id,
      },
    });
    const user_storage_info = await ctx.model.Quota.findOne({
      where: {
        user_id,
      },
    });
    console.log(user_storage_info);
    await ctx.model.User.update(
      { last_login_time: new Date() },
      {
        where: {
          user_id,
        },
      },
    );
    const userData = userInfo.dataValues;
    delete userData.password; // 删除密码属性
    return {
      profile: { ...user_storage_info.dataValues, ...userData },
    };
  }
}

module.exports = UserService;
