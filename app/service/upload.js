const { Service } = require('egg');
const uuid = require('node-uuid');
const fs = require('fs');
const bcryptjs = require('bcryptjs');
const { Op } = require('sequelize');
const path = require('path');
class UploadService extends Service {
  async saveStream(params) {
    try {
      const { ctx } = this;
      const { userId, part, targetPath, hash, fileSize, parent_folder_id } = params;
      console.log(params, 'params');
      const folderRes = await ctx.model.Folder.findOne({
        where: {
          folder_id: parent_folder_id === '1' ? null : parent_folder_id,
        },
        attributes: ['path', 'folder_name'],
      });
      const virtPath = folderRes
        ? folderRes.path === '/'
          ? `/${folderRes.folder_name}`
          : `${folderRes.path}/${folderRes.folder_name}`
        : '/';
      // console.log(vartPath, 'vartPath');
      const file_suffix = part.filename.split('.').pop();
      const file_name_upload = hash + path.extname(part.filename);
      const file_create_item = {
        file_id: uuid.v4(),
        file_name: part.filename,
        file_name_upload,
        file_path: path.join(targetPath, file_name_upload),
        file_size: fileSize,
        file_type: ctx.helper.tranFileType(file_suffix),
        file_suffix,
        user_id: userId,
        create_time: new Date(),
        modify_time: new Date(),
        parent_folder_id: parent_folder_id === '1' ? null : parent_folder_id,
        path: virtPath,
        hash,
      };
      await ctx.model.File.create(file_create_item);
      await ctx.service.file.calculationStorage({ user_id: userId });
      return;
    } catch (err) {
      console.log(err, 'err');
    }
  }
  async saveMergeChunks(params) {
    const { ctx } = this;
    const { userId, hash, fileName, fileSize, parent_folder_id } = params;
    console.log(parent_folder_id, 'parent_folder_id', 1);
    const folderRes = await ctx.model.Folder.findOne({
      where: {
        folder_id: parent_folder_id === '1' ? null : parent_folder_id,
      },
      attributes: ['path', 'folder_name'],
    });
    const virtPath = folderRes
      ? folderRes.path === '/'
        ? `/${folderRes.folder_name}`
        : `${folderRes.path}/${folderRes.folder_name}`
      : '/';
    const file_suffix = fileName.split('.').pop();
    console.log(file_suffix, 'file_suffix');
    const file_hash_name = hash + '.' + file_suffix;
    const file_create_item = {
      file_id: uuid.v4(),
      file_name: fileName,
      file_name_upload: file_hash_name,
      file_path: `app/public/upload/${userId}/${file_hash_name}`,
      file_size: fileSize,
      file_type: ctx.helper.tranFileType(file_suffix),
      file_suffix,
      user_id: userId,
      create_time: new Date(),
      modify_time: new Date(),
      parent_folder_id: parent_folder_id === '1' ? null : parent_folder_id,
      path: virtPath,
      hash,
    };
    await ctx.model.File.create(file_create_item);
    return;
  }
  async newFolder(params) {
    const { ctx } = this;
    const { parent_folder_id, folder_name } = params;
    const userId = ctx.state.user.data.user_id;
    console.log(userId, 'userId');
    try {
      const parentFolder = await ctx.model.Folder.findOne({
        where: {
          folder_id: parent_folder_id || null,
        },
      });
      const isFolderName = await ctx.model.Folder.findOne({
        where: {
          folder_name,
          parent_folder_id: parent_folder_id || null,
        },
      });
      if (isFolderName) {
        return {
          code: 210,
          message: '文件夹名称已存在',
        };
      }
      // const tranFolder_name = isFolderName ? folder_name + '-' + new Date().getTime() : folder_name;
      const depth = parentFolder ? parentFolder.depth + 1 : 0;
      const path = parentFolder
        ? parentFolder.path === '/'
          ? `/${parentFolder.folder_name}`
          : `${parentFolder.path}/${parentFolder.folder_name}`
        : '/';
      console.log(parentFolder, 'parentFolder');
      const folder_create_item = {
        folder_id: uuid.v4(),
        folder_name,
        user_id: userId,
        parent_folder_id: parentFolder ? parentFolder.folder_id : null,
        path,
        depth,
        create_time: new Date(),
        modify_time: new Date(),
      };
      const { folder_id } = await ctx.model.Folder.create(folder_create_item);
      return { folder_id, finish: !!folder_id };
    } catch (err) {
      console.log(err, 'err');
    }
  }
  async uploadAvatar(params) {
    const { ctx } = this;
    const { userId, avatar_url } = params;
    await ctx.model.User.update(
      { avatar_url },
      {
        where: {
          user_id: userId,
        },
      },
    );
  }
}
module.exports = UploadService;
