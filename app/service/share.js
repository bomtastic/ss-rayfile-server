const { Service } = require('egg');
const { Op } = require('sequelize');
const uuid = require('node-uuid');

class ShareService extends Service {
  async createShare(params) {
    const { ctx } = this;
    const { share_pwd, user_id, file_ids, folder_ids } = params;
    try {
      const share_id = uuid.v4();
      const share_object = {
        share_id,
        user_id,
        share_link: `http://localhost:9000/#/s/${share_id}`,
        can_download: true,
        create_time: new Date(),
        over_time: new Date(),
        views: 0,
        share_pwd,
      };
      await ctx.model.Share.create(share_object);
      if (file_ids instanceof Array) {
        for (let i = 0; i < file_ids.length; i++) {
          const share_files_object = {
            share_id,
            file_id: file_ids[i],
            folder_id: null,
          };
          await ctx.model.SharedFiles.create(share_files_object);
        }
      } else {
        const share_files_object = {
          share_id,
          file_id: file_ids,
          folder_id: null,
        };
        await ctx.model.SharedFiles.create(share_files_object);
      }

      return {
        share_link: share_object.share_link,
      };
    } catch (err) {
      console.log(err);
    }
  }

  async shareFile(params) {
    const { ctx } = this;
    const { share_pwd, share_id } = params;
    const shareFileData = await ctx.model.Share.findOne({
      include: {
        model: ctx.model.File,
        // attributes: ['file_name', 'file_id', 'file_type', 'file_suffix'],
      },
      where: {
        share_id,
      },
      // order: [['create_time', 'DESC']],
    });
    if (shareFileData.share_pwd) {
      if (share_pwd !== shareFileData.share_pwd) return;
    }
    await ctx.model.Share.increment(
      { views: 1 },
      {
        where: {
          share_id,
        },
      },
    );
    const { user_name } = await ctx.model.User.findOne({
      where: { user_id: shareFileData.user_id },
      attributes: ['user_name'],
    });
    return {
      shareFileData: {
        user_name,
        ...shareFileData.dataValues,
      },
    };
  }

  async ispwd(params) {
    const { ctx } = this;
    const { share_id } = params;
    const { share_pwd, user_id } = await ctx.model.Share.findOne({
      where: { share_id },
      attributes: ['share_pwd', 'user_id'],
    });
    const { user_name } = await ctx.model.User.findOne({
      where: { user_id },
      attributes: ['user_name'],
    });
    return { ispwd: !!share_pwd, user_name };
  }

  async userShareFile(params) {
    const { ctx } = this;
    const { user_id } = params;
    const shareData = await ctx.model.Share.findAll({
      include: {
        model: ctx.model.File,
        attributes: ['file_name', 'file_id', 'file_type', 'file_suffix'],
      },
      where: {
        user_id,
      },
      order: [['create_time', 'DESC']],
    });
    return shareData;
  }
  async saveDisk(params) {
    const { ctx } = this;
    const { user_id, share_id } = params;
    try {
      const shareData = await ctx.model.Share.findOne({
        include: {
          model: ctx.model.File,
          // attributes: ['file_name', 'file_id', 'file_type', 'file_suffix'],
        },
        where: {
          share_id,
        },
      });
      const { ev_shared_files, ...rest } = shareData.ev_files[0].dataValues;
      rest.file_id = uuid.v4();
      rest.user_id = user_id;
      rest.is_share = true;
      rest.path = '/';
      rest.parent_folder_id = null;
      const isSuccess = await ctx.model.File.create(rest);
      if (isSuccess) {
        return {
          isSuccess: !!isSuccess,
        };
      }
    } catch (err) {
      console.log(err);
    }
  }
}

module.exports = ShareService;
