const { Service } = require('egg');
const { Op } = require('sequelize');

class FileService extends Service {
  // 获取文件夹和文件
  async files(params) {
    const { fileType, path } = params;
    const { ctx } = this;
    // const folderId = params.folderId || null;
    const userId = ctx.state.user.data.user_id;
    console.log(path, 'path');
    console.log(userId, 'userId');
    let folder_id = null;
    try {
      if (path === '/') {
        folder_id = 1;
      } else {
        const parentFolderName = path.split('/').pop() || null;
        console.log(parentFolderName, 'parentFolderName');
        if (parentFolderName) {
          folder_id = await ctx.model.Folder.findOne({
            where: {
              folder_name: parentFolderName,
              user_id: userId,
            },
            attributes: ['folder_id'],
          });
          folder_id = folder_id.folder_id;
        }
      }
      const folders = await ctx.model.Folder.findAll({
        where: {
          parent_folder_id: folder_id === 1 ? null : folder_id,
          // path,
          user_id: userId,
        },
        order: [['create_time', 'DESC']],
      });
      const files = await ctx.model.File.findAll({
        where: {
          parent_folder_id: folder_id === 1 ? null : folder_id,
          // path,
          user_id: userId,
          file_type: fileType || { [Op.ne]: null },
        },
        order: [['create_time', 'DESC']],
      });
      return {
        folders,
        files,
        folder_id,
      };
    } catch (err) {
      console.log(err, 'err');
    }
  }
  async reFloderName(params) {
    const { ctx } = this;
    const { folder_id, folder_name } = params;
    try {
      await ctx.model.Folder.update(
        {
          folder_name,
          modify_time: new Date(),
        },
        {
          where: {
            folder_id,
          },
        },
      );
      return;
    } catch (err) {
      console.log(err, 'err');
    }
  }

  async reFileName(params) {
    const { ctx } = this;
    const { file_id, file_name } = params;
    try {
      await ctx.model.File.update(
        {
          file_name,
          modify_time: new Date(),
        },
        {
          where: {
            file_id,
          },
        },
      );
      return;
    } catch (err) {
      console.log(err, 'err');
    }
  }
  async backPic(params) {
    const { ctx } = this;
    const { file_id } = params;
    const { file_path } = await ctx.model.File.findOne({
      where: {
        file_type: 4,
        file_id,
      },
    });
    return file_path;
  }
  async deleteFile(params) {
    const { ctx } = this;
    const { file_id, user_id } = params;
    const { file_name_upload, file_path } = await ctx.model.File.findOne({
      where: {
        file_id,
      },
      attributes: ['file_name_upload', 'file_path'],
    });
    const filesCount = await ctx.model.File.findAll({
      where: {
        file_name_upload,
      },
    });
    const isSourceDelete = filesCount.length > 1;
    if (isSourceDelete) {
      await ctx.model.File.destroy({
        where: {
          file_id,
        },
      });
    }
    return {
      isSourceDelete: !isSourceDelete,
      file_path,
    };
  }
  async calculationStorage(params) {
    const { ctx } = this;
    const { user_id } = params;
    const allFileArr = await ctx.model.File.findAll({
      where: {
        user_id,
      },
      attributes: ['file_size'],
    });
    const used_spcae = allFileArr.reduce(
      (accumulator, { file_size }) => accumulator + file_size,
      0,
    );
    try {
      await ctx.model.Quota.update(
        {
          used_space: used_spcae,
        },
        {
          where: {
            user_id,
          },
        },
      );
    } catch (err) {
      console.log(err, 'err');
    }
    return used_spcae;
  }
  async category(params) {
    const { ctx } = this;
    const { currentPage, pageSize, user_id, file_type } = params;
    const res = await ctx.model.File.findAll({
      where: {
        file_type,
        user_id,
      },
      limit: pageSize, // 返回数据量
      offset: (currentPage - 1) * pageSize,
    });
    const count = await ctx.model.File.count({
      where: {
        file_type,
        user_id,
      },
    });
    console.log(count, 'count');
    return { list: res, count };
  }
}

module.exports = FileService;
