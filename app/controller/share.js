'use strict';

const BaseController = require('./base');

class ShareController extends BaseController {
  async createShare() {
    const { ctx, service } = this;
    const { share_pwd, file_ids, folder_ids } = ctx.request.body;
    const user_id = ctx.state.user.data.user_id;
    const allUserData = await service.share.createShare({
      user_id,
      share_pwd,
      file_ids,
      folder_ids,
    });
    this.success(allUserData);
  }
  async shareFile() {
    const { ctx, service } = this;
    const { share_id, share_pwd } = ctx.query;
    // const user_id = ctx.state.user.data.user_id;
    const shareFileData = await service.share.shareFile({ share_pwd, share_id });
    this.success(shareFileData);
  }
  async ispwd() {
    const { ctx, service } = this;
    const { share_id } = ctx.query;
    const ispwd = await service.share.ispwd({ share_id });
    this.success(ispwd);
  }
  async userShareFile() {
    const { ctx, service } = this;
    const user_id = ctx.state.user.data.user_id;
    const allShareData = await service.share.userShareFile({ user_id });
    this.success({ shareData: allShareData });
  }
  async saveDisk() {
    const { ctx, service } = this;
    const { share_id } = ctx.query;
    const user_id = ctx.state.user.data.user_id;
    const allShareData = await service.share.saveDisk({ user_id, share_id });
    this.success({ shareData: allShareData });
  }
}

module.exports = ShareController;
