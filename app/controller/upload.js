'use strict';

const BaseController = require('./base');
const error = require('egg').error;
const fs = require('fs');
const Pump = require('mz-modules/pump');
const path = require('path');
const uuid = require('node-uuid');
const sendToWormhole = require('stream-wormhole');
const crypto = require('crypto');
class UploadController extends BaseController {
  // 大文件上传
  async uploadChunk() {
    const UPLOADPATHTMP = 'app/public/tmp/upload';
    const { ctx } = this;
    const { parent_folder_id } = ctx.query;
    const userId = ctx.state.user.data.user_id;
    // const { filename, chunkIndex, totalNumber } = ctx.request.body;
    const stream = await this.ctx.getFileStream();
    console.log(stream.fields, 'stream.fields');
    const hash = stream.fields.hash;
    // 利用hash创建文件
    function mkdirsSync(hash) {
      if (fs.existsSync(hash)) {
        return true;
      }
      if (mkdirsSync(path.dirname(hash))) {
        fs.mkdirSync(hash);
        return true;
      }
    }
    mkdirsSync(path.join(UPLOADPATHTMP, userId, hash));
    // 生成写入路径
    const target = path.join(
      UPLOADPATHTMP,
      userId,
      hash,
      stream.fields.index + path.extname(stream.filename),
    );
    const writeStream = fs.createWriteStream(target);
    // 如果监听到取消 则删除文件
    this.ctx.req.on('aborted', () => {
      writeStream.close();
      fs.unlinkSync(target);
    });
    try {
      // 异步把文件流 写入
      await new Promise((resolve, reject) => {
        stream.pipe(writeStream).on('finish', resolve).on('error', reject);
      });
    } catch (err) {
      // 如果出现错误，消费掉整个流，关闭管道
      await sendToWormhole(stream);
      writeStream.close();
      this.ctx.body = {
        code: 500,
      };
    }
    this.success({
      code: 200,
      data: {},
    });
  }
  // 合并文件
  async mergeChunks() {
    const { ctx } = this;
    const userId = ctx.state.user.data.user_id;
    const { hash, fileName, parent_folder_id } = ctx.request.body;
    const UPLOADPATHTMP = 'app/public/tmp/upload';
    const UPLOADPATH = 'app/public/upload';
    const dirPath = path.join(UPLOADPATHTMP, userId, hash);
    const filePath = path.join(UPLOADPATH, userId, hash + path.extname(fileName));
    try {
      const files = fs.readdirSync(dirPath).sort();
      console.log(files, 'files');
      const targetStream = fs.createWriteStream(filePath);
      const readStream = files => {
        return new Promise((resolve, reject) => {
          const name = files.shift();
          const readFilePath = path.join(dirPath, name);
          const originStream = fs.createReadStream(readFilePath);
          // 依次将流写入通道
          originStream.pipe(targetStream, { end: false });
          originStream.on('end', () => {
            fs.unlinkSync(readFilePath);
            if (files.length > 0) {
              readStream(files).then(resolve).catch(reject);
            } else {
              targetStream.close();
              originStream.close();
              fs.rmdirSync(dirPath);
              resolve();
            }
          });
          originStream.on('error', reject);
        });
      };
      await readStream(files);
      console.log('合并完成');
      // 同步获取可能有问题
      const fileSize = fs.statSync(filePath).size;
      await this.service.upload.saveMergeChunks({
        userId,
        fileName,
        hash,
        fileSize,
        filePath,
        parent_folder_id,
      });
    } catch (e) {
      console.log(e, 'e');
      this.ctx.body = {
        code: 500,
      };
    }
    this.ctx.body = {
      code: 200,
      data: {
        url: `/download?filename=${fileName}&${hash + path.extname(fileName)}`,
      },
    };
  }
  // 查找是否存在该文件
  async checkFile() {
    const UPLOADPATH = 'app/public/upload';
    const { ctx } = this;
    const { hash, fileName } = ctx.request.body;
    const userId = ctx.state.user.data.user_id;
    const filePath = path.join(UPLOADPATH, userId, hash + path.extname(fileName));
    // 判断该该路径文件是否存在
    const isExist = fs.existsSync(filePath);
    this.success({
      code: 200,
      data: {
        isExist,
        url: `download?filename=${fileName}&${hash + path.extname(fileName)}`,
      },
    });
  }
  // 检查是否存在切片
  async getExistFileChunk() {
    const { ctx } = this;
    const UPLOADPATHTMP = 'app/public/tmp/upload';
    const userId = ctx.state.user.data.user_id;
    const { hash } = ctx.request.query;
    // 首先判断是否存在装切片的文件夹
    const dirPath = path.join(UPLOADPATHTMP, userId, hash);
    if (fs.existsSync(dirPath)) {
      // 读取该文件夹
      const files = fs.readdirSync(dirPath);
      this.success({
        code: 200,
        existChunk: files.map(item => +path.basename(item, path.extname(item))),
      });
    } else {
      this.success({
        code: 200,
        existChunk: [],
      });
    }
  }
  // 处理上传的文件，返回文件信息
  async handleFile(part, tempDir) {
    const filename = part.filename;
    const filepath = path.join(tempDir, filename);
    const writeStream = fs.createWriteStream(filepath);
    for await (const chunk of part) {
      writeStream.write(chunk);
    }
    writeStream.end();
    return { filename, filepath };
  }

  // 获取文件的大小和哈希值
  async getFileStats(filepath) {
    const stats = fs.statSync(filepath);
    const size = stats.size;
    const hash = await new Promise((resolve, reject) => {
      const hash = crypto.createHash('md5');
      const stream = fs.createReadStream(filepath);
      stream.on('error', reject);
      stream.on('data', chunk => {
        hash.update(chunk);
      });
      stream.on('end', () => {
        resolve(hash.digest('hex'));
      });
    });
    return { size, hash };
  }

  async uploadStream() {
    const { ctx } = this;
    const { parent_folder_id } = ctx.query;
    const userId = ctx.state.user.data.user_id;
    const UPLOADPATHTMP = `app/public/tmp/upload/${userId}`;
    const targetPath = `app/public/upload/${userId}`;
    const files = [];
    let hash;
    let part;
    let writeStream;
    let isExist = false;
    try {
      const parts = ctx.multipart({ autoFields: true });
      while ((part = await parts()) != null) {
        console.log(part, 'part', parts.field, 'parts.field');
        if (part.length) {
          // 如果文件为空，part是数组，说明是 fields
          console.log('field: ' + part[0]);
          console.log('value: ' + part[1]);
          console.log('valueTruncated: ' + part[2]);
          console.log('fieldnameTruncated: ' + part[3]);
        } else {
          // 用户没有选择文件就点击了上传，part 是 file stream，但是 part.filename 为空
          if (!part.filename) {
            continue;
          }
          hash = parts.field.hash;
          const file_name_upload = hash + path.extname(part.filename);
          if (fs.existsSync(targetPath)) {
            const dirPath = path.join(targetPath, file_name_upload);
            // 读取该文件
            isExist = await ctx.helper.dirExist(dirPath);
          } else {
            fs.mkdirSync(targetPath);
          }
          // const filename = uuid.v4() + path.extname(part.filename);
          // 处理文件
          // const { filename, filepath } = await this.handleFile(part, UPLOADPATHTMP);
          // const { size, hash } = await this.getFileStats(filepath);
          const target = path.join(this.config.baseDir, targetPath, file_name_upload);
          let fileSize = 0;
          if (!isExist) {
            const writeStream = fs.createWriteStream(target);
            files.push({
              [part.fieldname]: target, // es6里面的属性名表达式
            });
            // await part.pipe(writeStream);
            // const fileSize = await this.processPart(part, writeStream);
            part.on('data', chunk => {
              fileSize += chunk.length;
              console.log('chunk', chunk.length, fileSize);
            });
            part.on('end', () => {
              writeStream.end();
              console.log('end', fileSize);
            });
            part.on('error', err => {
              console.log(err, 'err');
            });
            await Pump(part, writeStream);
          } else {
            const { file_size } = await ctx.model.File.findOne({
              where: {
                hash,
              },
              attributes: ['file_size'],
            });
            fileSize = file_size;
          }
          console.log(fileSize, 'fileSize------------------');
          // 保存文件到指定路径
          await this.service.upload.saveStream({
            userId,
            part,
            targetPath,
            hash,
            fileSize,
            parent_folder_id,
          });
        }
      }
      console.log('success---------------------------------------');
      this.success({
        files,
        fields: parts.field,
        message: '上传成功',
      });
    } catch (err) {
      console.log('err---------------------------------------');
      console.error('文件写入错误', err);
      // 删除文件及数据库路数据
      // 必须将上传的文件流消费掉，要不然浏览器响应会卡死
      await sendToWormhole(part);
      writeStream.close();
      this.ctx.body = {
        code: 500,
      };
      throw err;
    }
  }
  async processPart(part, writeStream) {
    let fileSize = 0;
    return new Promise((resolve, reject) => {
      part.on('data', chunk => {
        fileSize += chunk.length;
        console.log('chunk', chunk.length, fileSize);
        // writeStream.write(chunk);
      });
      part.on('end', () => {
        console.log('end', fileSize);
        resolve(fileSize);
      });
      part.on('error', err => {
        console.log(err, 'err');
        reject(err);
      });
    });
  }
  async newFolder() {
    const { ctx } = this;
    const userId = ctx.state.user.data.user_id;
    console.log(userId, 'userId');
    const res = await this.service.upload.newFolder(ctx.request.body);
    this.success(res);
  }
  async uploadAvatar() {
    const { ctx } = this;
    const userId = ctx.state.user.data.user_id;
    const targetPath = `app/public/avatar/${userId}`;
    const stream = await ctx.getFileStream();
    const filename = stream.filename;
    console.log(filename, 'filename');
    // 将上传的文件保存到本地
    const writeTarget = path.join(targetPath, filename);
    console.log(writeTarget, 'writeTarget');
    const writeStream = fs.createWriteStream(writeTarget);
    if (!fs.existsSync(targetPath)) {
      fs.mkdirSync(targetPath);
    }
    try {
      await new Promise((resolve, reject) => {
        stream.pipe(writeStream).on('finish', resolve).on('error', reject);
      });
    } catch (err) {}
    const avatar_url = ctx.request.origin + `/avatar/${userId}/${filename}`;
    ctx.service.upload.uploadAvatar({ userId, avatar_url });
    this.success({
      code: 200,
      message: '上传成功',
      data: {
        url: avatar_url,
      },
    });
  }
}

module.exports = UploadController;
