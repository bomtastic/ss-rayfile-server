'use strict';

const { Controller } = require('egg');

class HomeController extends Controller {
  async index() {
    const { ctx, app } = this;
    console.log('index');
    // await app.redis.set('hello', '你好');
    app.redis.set('hello', '你好', 'EX', 10 * 60);
    ctx.body = await app.redis.get('hello');
  }
}

module.exports = HomeController;
