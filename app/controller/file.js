'use strict';

const BaseController = require('./base');
const fs = require('fs');
const path = require('path');
const archiver = require('archiver');
const sharp = require('sharp');
const stream = require('stream');

class FileContraller extends BaseController {
  async calculationStorage() {
    const { ctx, service } = this;
    const user_id = ctx.state.user.data.user_id;
    const res = await service.file.calculationStorage({ user_id });
    this.success({
      res,
    });
  }
  async files() {
    const { ctx, service } = this;
    const queries = ctx.query;
    // console.log('URL:', ctx.url);
    // console.log('Method:', ctx.method);
    // console.log('Headers:', ctx.headers);
    // console.log(queries, 'queries');
    const res = await service.file.files(queries);
    this.success(res);
  }
  async download() {
    const { ctx, service } = this;
    const fileIds = ctx.queries.fileId;
    console.log(fileIds, 'fileIds', fileIds.length);
    if (fileIds.length > 1) {
      const archive = archiver('zip', {
        zlib: { level: 9 },
      });
      // 添加文件到压缩文件
      // fileIds.forEach(fileId => {
      //   const { file_path } = ctx.model.File.findOne({
      //     where: { fileId },
      //     attributes: ['file_path'],
      //   }); // 获取文件的路径
      //   console.log(file_path, 'filePath');
      //   archive.append(fs.createReadStream(file_path), { name: path.basename(file_path) });
      // });
      try {
        const files = await Promise.all(
          fileIds.map(async fileId => {
            const file = await ctx.model.File.findOne({
              where: { file_id: fileId },
              attributes: ['file_path', 'file_name'],
            }); // 获取文件的路径
            return file;
          }),
        );
        console.log(files, 'filePaths');
        files.forEach(file => {
          archive.append(fs.createReadStream(file.file_path), {
            name: path.basename(file.file_name),
          });
        });
      } catch (err) {
        console.log(err, 'err');
      }
      ctx.set('Content-Type', 'application/octet-stream'); // 设置响应的类型为流
      ctx.set('Content-Disposition', `attachment; filename="${encodeURIComponent('files.zip')}"`); // 设置下载的文件名
      ctx.set('filename', encodeURIComponent('files.zip'));
      ctx.body = archive; // 设置响应体为压缩文件的流

      archive.finalize(); // 完成压缩文件的创建
    } else {
      try {
        const { file_path, file_name } = await ctx.model.File.findOne({
          where: { file_id: fileIds[0] },
          attributes: ['file_path', 'file_name'],
        });
        const filePath = path.join(this.config.baseDir, file_path);
        // const stream = fs.createReadStream(filePath); // 创建一个读取流
        // const filename = encodeURIComponent(path.basename(filePath)); // 对文件名进行编码
        // ctx.set('Content-Disposition', `attachment; filename*=UTF-8''${filename}`);
        ctx.attachment(path.basename(filePath));
        ctx.set('Content-Type', 'application/octet-stream'); // 设置响应的类型为流
        // ctx.set('Content-Disposition', `attachment; filename="${encodeURIComponent(file_name)}"`); // 设置下载的文件名
        ctx.set('filename', encodeURIComponent(file_name));
        // stream.on('error', err => {
        //   console.error('Error while reading file:', err);
        // });
        ctx.body = fs.createReadStream(filePath); // 设置响应体为读取流
      } catch (err) {
        console.log(err, 'err');
      }
    }

    // const res = await service.file.download({ fileId });
    // this.success(res);
  }
  async reFloderName() {
    const { ctx, service } = this;
    const params = ctx.request.body;
    const res = await service.file.reFloderName(params);
    this.success(res);
  }
  async reFileName() {
    const { ctx, service } = this;
    const params = ctx.request.body;
    const res = await service.file.reFileName(params);
    this.success(res);
  }

  async resizeImage({ file_path, x = 200, y = 200 }) {
    // if (!fs.existsSync(filePath)) {
    //   throw new Error(`File not found: ${filePath}`);
    // }
    const allFilePath = path.join(this.config.baseDir, file_path);
    // 使用 sharp 库处理图片
    const image = sharp(allFilePath);
    // console.log(image, 'image');
    try {
      const resizedImageBuffer = await image.resize(Number(x), Number(y)).toBuffer();
      // console.log(resizedImageBuffer, 'resizedImageBuffer');
      // 创建一个可读的 Stream
      const readableStream = new stream.PassThrough();
      readableStream.end(resizedImageBuffer);
      return resizedImageBuffer;
    } catch (err) {
      console.log(err, 'err');
    }
  }
  async backPic() {
    const { ctx } = this;
    // const pathArr = ctx.params.imgPath;
    const { file_id, x, y } = ctx.query;
    console.log(file_id, 'file_id', ctx.query, 'ctx.query');
    console.log(this.config.baseDir, 'baseDir');
    // const filePath = path.join(this.config.baseDir, pathArr);
    // console.log(path.basename(), path.dirname());
    // console.log(pathArr, 'pathArr');
    const file_path = await ctx.service.file.backPic({ file_id });
    const imageStream = await this.resizeImage({ file_path, x, y });
    ctx.body = imageStream;
    ctx.type = 'image/jpeg';
  }
  async deleteFile() {
    try {
      const { ctx } = this;
      const { file_id } = ctx.query;
      const user_id = ctx.state.user.data.user_id;
      const { isSourceDelete, file_path } = await ctx.service.file.deleteFile({ file_id });
      console.log(file_path, '-------file_path-----');
      if (isSourceDelete) {
        fs.promises
          .access(file_path, fs.constants.R_OK || fs.constants.W_OK)
          .then(() => {
            console.log(
              `-----------------------${file_path} is readable and writable---------------------`,
            );
          })
          .catch(err => {
            console.log(
              `-----------------------${file_path} ${err ? 'is not' : 'is'} readable and writable---------------------`,
            );
          });
        fs.unlink(file_path, err => {
          if (err) throw err;
          console.log('path/file.txt was deleted');
          // await ctx.model.File.destroy({
          //   where: {
          //     file_id,
          //   },
          // });
          // await ctx.service.file.calculationStorage({ user_id });
        });
        // try {
        //   fs.unlinkSync(file_path);
        // } catch (err) {
        //   console.log(err, 'err');
        // }
      }
      this.success({
        isSourceDelete,
        file_id,
      });
    } catch (err) {
      console.log(err, 'err');
      this.err();
    }
  }
  async deleteFile1() {
    const { ctx } = this;
    const { file_path } = ctx.query;
    await fs.promises
      .unlink(file_path)
      .then(() => {
        // ctx.model.File.destroy({
        //   where: {
        //     file_id,
        //   },
        // });
        console.log('success');
      })
      .catch(err => console.log(err));
    // try {
    //   fs.unlinkSync(file_path);
    // } catch (err) {
    //   console.log(err, 'err');
    // }
  }
  // async category() {
  //   const { ctx } = this;
  //   const cat = Number(ctx.query.file_type);
  //   await this.displayPic(ctx.query);
  //   // if (cat === 4) await this.displayPic(ctx.query);
  //   // if (cat === 3) await this.displayDoc(ctx.query);
  // }
  async category() {
    const { ctx, service } = this;
    const user_id = ctx.state.user.data.user_id;
    const query = ctx.query;
    const currentPage = parseInt(query.page) || 1; // 默认当前页为 1
    const pageSize = parseInt(query.pageSize) || 50; // 默认每页数量为 10
    const file_type = parseInt(query.file_type);
    const resArr = await service.file.category({ currentPage, pageSize, user_id, file_type });
    this.success(resArr);
  }
}

module.exports = FileContraller;
