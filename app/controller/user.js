'use strict';

// const { Controller } = require('egg');
const BaseController = require('./base');
const bcryptjs = require('bcryptjs');

class UserController extends BaseController {
  // async getTokenInfo() {
  //   const { ctx, service, app } = this;
  //   const token = ctx.request.header.authorization;
  //   const decode = await app.jwt.verify(token, app.config.jwt.secret);
  //   ctx.body = { decode };
  //   console.log(token, decode, 'token');
  // }
  async getAllUser() {
    const { ctx, service } = this;
    const allUserData = await service.user.getAllUser();
    console.log(ctx.state.user, 'header');
    this.success(allUserData);
  }
  async valEmail() {
    const { ctx, service } = this;
    const data = ctx.request.body;
    const res = await service.user.valEmail(data);
    this.success(res);
  }
  async sendEmail() {
    const { ctx, service } = this;
    const data = ctx.request.body;
    const res = await service.user.sendEmail(data);
    this.success(res);
  }
  async userRegister() {
    const { ctx, service } = this;
    const params = ctx.request.body;
    const { user_name, password, email, phone } = params;
    const encipherPassword = bcryptjs.hashSync(password, 10);
    const res = await service.user.userRegister({ user_name, encipherPassword, email, phone });
    // const valEmail=await service.user.valEmail(email);
    this.success(res);
  }
  async userLogin() {
    const { ctx, service } = this;
    // this.ctx.cookies.set('username', '张三', {
    //   // 设置cookie的有效期
    //   maxAge: 1000 * 3600 * 24,
    //   // 只允许服务端访问cookie
    //   httpOnly: true,
    //   // 对cookie进行签名，防止用户修改cookie
    //   signed: true,
    //   // 是否对cookie进行加密
    //   // cookie加密后获取的时候要对cookie进行解密
    //   // cookie加密后就可以设置中文cookie
    //   encrypt: true,
    // });
    // console.log(ctx.cookies.get('test'), 'cookies');
    const data = ctx.request.body;
    const allUserData = await service.user.userLogin(data);
    this.success(allUserData);
  }
  async loginStatus() {
    // const { ctx, service } = this;
  }
  async getAcount() {
    const { ctx, service } = this;
    const user_id = ctx.state.user.data.user_id;
    const profile = await service.user.getAcount({ user_id });
    this.success(profile);
  }
}

module.exports = UserController;
