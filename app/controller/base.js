// app/controller/base.js
const Controller = require('egg').Controller;
class BaseController extends Controller {
  success(data = null, code = 200, message = 'ok') {
    const { ctx } = this;
    ctx.status = 200;
    ctx.body = {
      code,
      message,
      data,
    };
  }
  err(data = null, code = 500, message = 'error') {
    const { ctx } = this;
    ctx.status = 200;
    ctx.body = {
      code,
      message,
      data,
    };
  }
}
module.exports = BaseController;
