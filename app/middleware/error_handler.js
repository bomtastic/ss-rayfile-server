module.exports = () => {
  return async (ctx, next) => {
    const method = ctx.request.method;
    // const token = ctx.request.header.authorization;
    // console.log(token, '-----------------');

    try {
      await next();
      if (method === 'OPTIONS') {
        ctx.status = 204;
        return;
      }
    } catch (err) {
      if (err.name === 'UnauthorizedError') {
        ctx.body = {
          code: 401,
          message: '请先登录',
          data: err,
        };
      }
    }
  };
};
