'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.get('/api/getAllUser', controller.user.getAllUser);
  router.post('/userLogin', controller.user.userLogin);
  router.post('/userRegister', controller.user.userRegister);
  // router.get('/user/token', controller.user.getTokenInfo);
  router.post('/valEmail', controller.user.valEmail);
  router.post('/sendEmail', controller.user.sendEmail);
  router.post('/api/uploadStream', controller.upload.uploadStream);
  router.post('/api/newFolder', controller.upload.newFolder);
  router.get('/api/files', controller.file.files);
  router.get('/files/download', controller.file.download);
  router.post('/api/reFloderName', controller.file.reFloderName);
  router.post('/api/reFileName', controller.file.reFileName);
  router.get('/pic/:imgPath*', controller.file.backPic);
  router.post('/api/uploadChunk', controller.upload.uploadChunk);
  router.get('/api/getExistFileChunk', controller.upload.getExistFileChunk);
  router.post('/api/mergeChunks', controller.upload.mergeChunks);
  router.post('/api/checkFile', controller.upload.checkFile);
  router.post('/api/uploadAvatar', controller.upload.uploadAvatar);
  router.get('/api/deleteFile', controller.file.deleteFile);
  router.get('/api/deleteFile1', controller.file.deleteFile1);
  router.get('/api/calculationStorage', controller.file.calculationStorage);
  router.get('/login/status', controller.user.loginStatus);
  router.get('/api/getAcount', controller.user.getAcount);
  router.get('/api/category', controller.file.category);
  router.post('/api/createShare', controller.share.createShare);
  router.get('/api/shareFile', controller.share.shareFile);
  router.get('/api/userShareFile', controller.share.userShareFile);
  router.get('/api/ispwd', controller.share.ispwd);
  router.get('/api/saveDisk', controller.share.saveDisk);

  
};
