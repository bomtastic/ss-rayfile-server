/* eslint valid-jsdoc: "off" */

'use strict';
const path = require('path');
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {});

  config.sequelize = {
    dialect: 'mysql', // support: mysql, mariadb, postgres, mssql
    database: 'ssrayfile', // 数据库名
    host: '127.0.0.1',
    port: 3306,
    username: 'root', // mysql 用户
    password: '123456', // 密码
    define: {
      underscored: true,
      freezeTableName: true,
      timestamps: false,
    },
    timezone: '+08:00', // 中国时区
    dialectOptions: {
      dateStrings: true,
      typeCast(field, next) {
        // 对 date 类型进行特殊处理
        if (field.type === 'DATETIME' || field.type === 'TIMESTAMP') {
          return field.string();
        }
        return next();
      },
    },
  };

  exports.redis = {
    client: {
      host: 'localhost',
      port: 6379,
      password: '',
      db: 0,
    },
  };

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1699454650261_627';

  config.email = {
    host: 'smtp.163.com', // 服务 smtp 163邮箱
    port: 465, // smtp端口 默认无需改动
    secure: true,
    auth: {
      user: 'jwt1797247040@163.com', // 用户名
      pass: 'RLDVVTWQQAXGTIXV', // SMTP授权码
    },
    // tls: {
    //     rejectUnauthorized: false
    // }
  };

  config.security = {
    csrf: {
      enable: false,
    }, // 必须加 否则： 403 Forbidden message: "missing csrf token"
    domainWhiteList: ['http://localhost:9000'],
  };
  config.jwt = {
    secret: '123456',
    enable: true,
    match: '/api',
    sign: {
      expiresIn: 60 * 60 * 24 * 7, // 一周过期
      // expiresIn: 60, // 一周过期
    },
  };
  // config.bcrypt = { saltRounds: 10 };
  config.cors = {
    origin: 'http://localhost:9000',
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
    credentials: true,
    allowHeaders: ['Content-Type', 'Authorization'],
    exposeHeaders: ['Content-Disposition', 'filename'],
  };
  config.bodyParser = {
    multipart: true,
    enable: true,
    jsonLimit: '10mb', // JSON 请求体大小限制
    formLimit: '10mb', // 表单请求体大小限制
    textLimit: '10mb', // 文本请求体大小限制
    extendTypes: {
      // 在此处添加任何其他 MIME 类型的大小限制，例如：
      'multipart/form-data': '2000mb', // 如果你使用 multipart/form-data 进行文件上传
    },
  };

  config.multipart = {
    mode: 'stream',
    // 表单 Field 文件名长度限制
    fieldNameSize: 100,
    // 表单 Field 内容大小
    fieldSize: '100kb',
    // 表单 Field 最大个数
    fields: 10,
    // 单个文件大小
    fileSize: '200mb',
    // 允许上传的最大文件数
    files: 10,
    fileExtensions: [
      '.txt',
      '.docx',
      '.doc',
      '.ppt',
      '.pptx',
      '.xls',
      '.xlsx',
      '.pdf',
      '.jpg',
      '.jpeg',
      '.png',
      '.gif',
      '.mp4',
      '.mp3',
      '.zip',
      '.rar',
      '.7z',
      '.tar',
      '.gz',
      '.tgz',
      '.bz2',
      '.apk',
      '.ipa',
      '.dmg',
      '.exe',
      '.msi',
      '.iso',
      '.torrent',
      '.md',
      '.markdown',
      '.html',
      '.htm',
      '.css',
      '.js',
      '.json',
      '.xml',
      '.svg',
      '.ico',
      '.webp',
      '.woff',
      '.woff2',
      '.ttf',
      '.eot',
      '.otf',
      '.mp3',
      '.wav',
      '.flac',
      '.ape',
      '.ogg',
      '.m4a',
      '.aac',
      '.wma',
      '.flv',
      '.avi',
      '.mpg',
      '.mpeg',
      '.mov',
      '.rm',
      '.rmvb',
      '.wmv',
      '.mkv',
      '.asf',
      '.mp4',
      '.m4v',
      '.f4v',
      '.f4p',
      '.f4a',
      '.f4b',
      '.webm',
      '.m3u8',
      '.m3u',
      '.ts',
      '.3gp',
      '.3g2',
      '.m2ts',
      '.mts',
      '.m2t',
      '.m2p',
      '.m2v',
      '.m2a',
      '.m2a',
      '.m2b',
      '.m2f',
      '.m2r',
      '.m2s',
      '.m2v',
      '.m2t',
      '.m2p',
      '.m2a',
      '.m2a',
      '.m2b',
      '.m2f',
      '.m2r',
      '.m2s',
      '.m2v',
      '.m2t',
      '.m2p',
      '.m2a',
      '.m2a',
      '.m2b',
      '.m2f',
      '.m2r',
      '.m2s',
      '.m2v',
      '.m2t',
      '.m2p',
      '.m2a',
      '.m2a',
      '.m2b',
      '.m2f',
      '.m2r',
      '.m2s',
      '.m2v',
      '.m2t',
      '.m2p',
      '.m2a',
      '.m2a',
      '.m2b',
      '.m2f',
      '.m2r',
      '.m2s',
    ],
  };
  config.static = {
    prefix: '/', // 访问路径前缀
    dir: path.join(appInfo.baseDir, 'app/public'), // 静态文件目录
    dynamic: true, // 如果文件不存在，不返回 404，而是交给后续中间件处理
    preload: false, // 是否预加载
    buffer: false, // 是否缓存
    maxFiles: 1000, // 缓存文件的最大数量
  };
  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
    middleware: ['errorHandler', 'response'],
  };

  return {
    ...config,
    ...userConfig,
  };
};
